import logo from './logo.svg';
import TableExample from './TableExample';


function App() {
  return (
    <div className="App">
      <TableExample/>
    </div>
  );
}

export default App;
